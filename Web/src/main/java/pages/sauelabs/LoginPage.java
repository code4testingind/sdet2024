package pages.sauelabs;

import Infra.BasePage;
import Infra.SeleniumTest;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    /*
        1. Username

        2. Password

        3. Login Button

        4. Swag Labs Text logo

        5. Negative Scenario Validation Messages


     */


    // Sample format of creating locators for Login Screen
    private static By username      = By.id("user-name");
    private static By pwd      = By.name("password");
    private static By loginButton   = By.id("login-button");
    private static By invalid_user_login_messgae = By.xpath("//h3[contains(text(),'Epic sadface: Username and password do not match a')]");



    // Login Page operations

    public void enterUsername(String input){
        sendKeys(username, input);
    }

    public void clearUsername(){
        clearText(username);
    }

    public void enterPassword(String password){
        sendKeys(pwd, password);
    }

    public void clickLogin(){

        click(loginButton);
    }


    public String getLoginErrorMessage(){
       return getUIText(invalid_user_login_messgae);
    }


    // Re-usable methods
    public void login(String un, String pwd){

        // impl
    }



}
