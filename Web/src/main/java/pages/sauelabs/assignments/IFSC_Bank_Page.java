package pages.sauelabs.assignments;

import Infra.BasePage;
import com.saucelabs.common.utils.logs.TestLogger;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class IFSC_Bank_Page extends BasePage {

    Logger logger;

    public IFSC_Bank_Page(){
        logger = TestLogger.getLogger(IFSC_Bank_Page.class.getName());
    }

    private static By bank_dropdown         = By.xpath("//select[contains(@onchange,'location.href=bank.SelectURL')]");
    private static By state_dropdown        = By.xpath("//select[contains(@onchange,'location.href=state.SelectURL')]");
    private static By district_dropdown     = By.xpath("//select[contains(@onchange,'location.href=district.SelectURL')]");
    private static By branch_dropdown       = By.xpath("//select[contains(@onchange,'location.href=branch.SelectURL')]");
    private static By IFSC_Code_locator            = By.xpath("//b[text()='IFSC Code:']/following-sibling::a");

    //Select Bank
    public void selectBank(String bankName) throws InterruptedException {
        selectDropdownOptionWithVisibleText(bank_dropdown, bankName);
        String selected_Option = getSelectedOption(bank_dropdown);
        Assert.assertNotNull(selected_Option,"Bank dropdown is not selected...");
        logger.info("Selected option in Bank dropdown is: "+selected_Option);
    }

    // Select State
    public void selectState(String stateName) throws InterruptedException {
        selectDropdownOptionWithVisibleText(state_dropdown, stateName);
        String selected_Option = getSelectedOption(state_dropdown);
        Assert.assertNotNull(selected_Option,"State dropdown is not selected...");
        logger.info("Selected option in state dropdown is: "+selected_Option);

    }

    // Select District
    public void selectDistrict(String distrctName) throws InterruptedException {
        selectDropdownOptionWithVisibleText(district_dropdown, distrctName);
        String selected_Option = getSelectedOption(district_dropdown);
        Assert.assertNotNull(selected_Option,"Distrcit dropdown is not selected...");
        logger.info("Selected option in Distrcit dropdown is: "+selected_Option);
    }

    // Select Branch
    public void selectBranch(String branchName) throws InterruptedException {
        selectDropdownOptionWithVisibleText(branch_dropdown, branchName);
        String selected_Option = getSelectedOption(state_dropdown);
        Assert.assertNotNull(selected_Option,"Branch dropdown is not selected...");
        logger.info("Selected option in Branch dropdown is: "+selected_Option);
    }

    // Get IFSC Code
    public String get_IFSC_Code(){
        String ifsc_code =  getUIText(IFSC_Code_locator);
        logger.info("IFSC Code is: "+ ifsc_code );
        Assert.assertNotNull(ifsc_code,"IFSC Code is Empty...");
        return ifsc_code;
    }



}
