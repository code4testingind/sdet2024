package pages.sauelabs;

import Infra.BasePage;
import org.openqa.selenium.By;

public class ProductsPage extends BasePage  {

    // Products Screen locators
    /*

         1. Add to Cart

         2. Sorting Dropdown

         3. Name_Sort(A_Z)

         4. Name_Sort(Z_A)

         5. Price_Sort(Low_High)

         6. Price_Sort(High_Low)

         7. Products Label

         8. Main_Menu

         9. Menu_AllItem

         10. Menu_About

         11. Menu_Reset_App_State

         12. Remove Item from Cart

     */

    private static By sort_dropdown = By.className("product_sort_container");


    public void sortTheProducts_with_Price_High_To_Low(String sortingOrder) throws InterruptedException {

        selectDropdownOptionWithVisibleText(sort_dropdown, sortingOrder);
    }


}
