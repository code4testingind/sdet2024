package Infra;

import com.saucelabs.common.utils.logs.TestLogger;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class BasePage {



    public void launchURL(String url){
        SeleniumTest.getDriver().get(url);
    }

    public void click(By locator){

        SeleniumTest.getDriver().findElement(locator).click();


    }

    public void sendKeys(By locator, String text){

        SeleniumTest.getDriver().findElement(locator).sendKeys(text);
    }

    public void clearText(By locator){
        SeleniumTest.getDriver().findElement(locator).clear();
    }

    public void waitForPageToLoad(){

        // New Syntax for Implicit - for complete page from Selenium 4.x
        SeleniumTest.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Old Syntax
       // SeleniumTest.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void selectDropdownOptionWithVisibleText(By locator, String inputVisibleText) throws InterruptedException {

        //1 - Identification of Webelement
        WebElement dropdown = SeleniumTest.getDriver().findElement(locator);

        //2- Create object for the select Class
        Select select = new Select(dropdown);

        // Perform operations
        select.selectByVisibleText(inputVisibleText);
        Thread.sleep(3000);

    }

    public String getSelectedOption(By locator){
        //Assert the selected value
        WebElement dropdown = SeleniumTest.getDriver().findElement(locator);
        Select select = new Select(dropdown);
        WebElement selected_Option = select.getFirstSelectedOption();
        return selected_Option.getText();
    }

    public void selectDropdownOptionWithIndex(By locator, int inputIndex){
        //1 - Identification of Webelement
        WebElement dropdown = SeleniumTest.getDriver().findElement(locator);

        //2- Create object for the select Class
        Select select = new Select(dropdown);

        // Perform operations
        select.selectByIndex(inputIndex);
    }

    public void selectDropdownOptionWithValue(By locator, String inputValueText){

        //1 - Identification of Webelement
        WebElement dropdown = SeleniumTest.getDriver().findElement(locator);

        //2- Create object for the select Class
        Select select = new Select(dropdown);

        // Perform operations
        select.selectByVisibleText(inputValueText);

    }

    public void deSelectDropdownWithVisibleText(By locator, String inputText_To_deSlect){

        //1 - Identification of Webelement
        WebElement dropdown = SeleniumTest.getDriver().findElement(locator);

        //2- Create object for the select Class
        Select select = new Select(dropdown);

        // Perform operations
        select.deselectByVisibleText(inputText_To_deSlect);
    }

    public String getUIText(By locator){
        return SeleniumTest.getDriver().findElement(locator).getText();
    }

}
