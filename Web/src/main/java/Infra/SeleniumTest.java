package Infra;

import com.saucelabs.common.utils.logs.TestLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Locale;

public class SeleniumTest {


    public static WebDriver driver;

    private static SeleniumTest seleniumTest = null;

    private static Logger logger;

    public static SeleniumTest getInstance(){

        logger = TestLogger.getLogger(SeleniumTest.class.getName());
        if(seleniumTest == null){
            seleniumTest = new SeleniumTest();
        }
        return seleniumTest;
    }

    public void launchBrowser(String browser){

        String systemOS = getSystemOS().toLowerCase();

        if(browser.equalsIgnoreCase("Chrome")) {

            if(systemOS.equalsIgnoreCase("mac os x") || systemOS.startsWith("windows")){
                logger.info("System OS is: "+ systemOS);

                WebDriverManager.chromedriver().setup();

                //Customize the default download directoyr - ChromeOptions
                ChromeOptions options = new ChromeOptions();

                String downladpath = System.getProperty("user.dir");
                options.setCapability("chrome.download.directory","downladpath");

                // Launch browser with Custom profile - ChromeOptions
                driver = new ChromeDriver();


            }else if(systemOS.equalsIgnoreCase("linux")){

                // Note: Launch the browser in Headless Mode in Linux mode
                logger.info("System OS is: "+ systemOS);

                // Launch Browser in headless mode
                ChromeOptions chromeOptions = new ChromeOptions();
                WebDriverManager.chromedriver().setup();

                chromeOptions.addArguments("--no-sandbox");
                chromeOptions.addArguments("--headless");
                chromeOptions.addArguments("--disable-dev-shm-usage");
                chromeOptions.addArguments("--window-size=1920x1080");

                driver = new ChromeDriver(chromeOptions);
                logger.info("Chrome browser is launched in Headless mode in Linux Server...!");

            }else if(browser.equalsIgnoreCase("Firefox")){
                
                logger.info("Browser value is: "+ browser);
                logger.info("System OS is: "+ systemOS);

                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();


            } else if(browser.equalsIgnoreCase("Edge")) {

                // Edge Browser Code

            } else {
                System.out.println("No browser keyword is mtaching..");
            }
        }
    }

    private String getSystemOS(){
        return System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
    }
    public static WebDriver getDriver(){
        return driver;
    }

    public void nonstatic(){

    }
}
