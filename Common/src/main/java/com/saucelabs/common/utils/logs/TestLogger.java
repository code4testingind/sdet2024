package com.saucelabs.common.utils.logs;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestLogger {

    private static Logger logger;

    public static Logger getLogger(String className){

        logger = LogManager.getLogger(className);
        return logger;
    }

    public void info(String message){
        logger.info(message);
    }

    public void info(Exception e){
        logger.info(e);
    }
    public void error(String message){
        logger.error(message);
    }

    public void error(Exception e){
        logger.error(e);
    }
}
