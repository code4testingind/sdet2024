package com.saucelabs.common.utils.fileutils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropManager {

    String configFilePath;
    public PropManager(String configFilePath){
        this.configFilePath = configFilePath;
    }

    public String getProperty(String key) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(configFilePath));

        // we have to load second file also

        return props.getProperty(key);
    }
}
