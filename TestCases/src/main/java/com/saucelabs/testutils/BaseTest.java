package com.saucelabs.testutils;

import Infra.SeleniumTest;
import com.saucelabs.common.utils.fileutils.PropManager;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;

    public class BaseTest {

       protected PropManager propManager;
       String configFilePath = System.getProperty("user.dir") + "/Config/CONFIG.properties";

    public BaseTest(){
        propManager = new PropManager(configFilePath);
    }


    @BeforeMethod
    public void beforeMethod() throws IOException {

        System.out.println("beforeMethod() from BaseTest...");

        SeleniumTest.getInstance().launchBrowser("Chrome");
        SeleniumTest.getDriver().get(propManager.getProperty("url"));

    }


    @AfterMethod
        public void afterMethod(){

        System.out.println("afterMethod() from BaseTest...");

        // Close the browser
        SeleniumTest.getDriver().close();
    }


}
