package com.saucelabs.tests.Login;

public  class ArrayDemoTest {

    public static void main(String[] args) {

        // declare the array
        int[] arr;

        arr = new int[5];


        arr[0] = 12;

        arr[1] = 13;

        arr[2] = 14;

        arr[3] = 15;

        arr[4] = 16;

       // arr[5] = 17; -- ArrayIndexOutOfBoundException - as this memory location is not available

        // alternate way to store the values

        int[] arr_02 = {1,2,3,4,5};


        // Read the array values

    System.out.println("Array values are: " + arr[2]);

    System.out.println("Array size is: " + arr.length);

    // Read the array value using for loop

        for(int i=0; i< arr.length; i++){

            if(i == 4){
                System.out.println("index-4 value is "+arr[i]);
            }

        }

        int temp =0 ;
        //For each loop
        for(int j: arr){

            if(temp == 4){
                System.out.println("array values from for-each loop: " + j);
            }

            temp++;
        }


    }
}
