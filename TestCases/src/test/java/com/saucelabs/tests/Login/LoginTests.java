package com.saucelabs.tests.Login;


import org.testng.annotations.Test;

public class LoginTests extends LoginBaseTest {

// simple login tests
   @Test(priority = 1, description = "loginto saucelabs")
    public void loginToSaucelabsTest() throws InterruptedException {

       // System.out.println("Saucelabs login test execution started..!");

        logger.info("Login test execution started..");

        loginPage.enterUsername("standard_user");

        loginPage.enterPassword("secret_sauce");

        loginPage.clickLogin();

        // Wait statements
        productsPage.waitForPageToLoad();

        Thread.sleep(5000);

        // Sort the products by usiing sorting dropdown - Sort with Pirce high to low

        productsPage.sortTheProducts_with_Price_High_To_Low("Price (high to low)");

        Thread.sleep(5000);

       logger.info("Login test execution stopped..");

    }

    @Test(priority = 2, description = "Login Test with Invalid credentials")
    public void invalidLoginTest(){

        logger.info("Login test execution started..");

        loginPage.enterUsername("standard_user");

        loginPage.enterPassword("secret_sauce12233");

        loginPage.clickLogin();

       logger.info( loginPage.getLoginErrorMessage());

    }
}
