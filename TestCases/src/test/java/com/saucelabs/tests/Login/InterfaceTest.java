package com.saucelabs.tests.Login;

public interface InterfaceTest {


    // simple abstract method
    public void add();



}

class A implements InterfaceTest{


    @Override
    public void add() {

    }
}

class B implements InterfaceTest{


    @Override
    public void add() {
       // adding business logic as per the Client-B
    }


    //Nomraml method
    public void display()
    {
        System.out.println("display()");
    }
}