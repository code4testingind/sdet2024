package com.saucelabs.tests.Login;

import Infra.SeleniumTest;
import com.saucelabs.common.utils.logs.TestLogger;
import com.saucelabs.testutils.BaseTest;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import pages.sauelabs.LoginPage;
import pages.sauelabs.ProductsPage;

public class LoginBaseTest extends BaseTest {

    protected LoginPage loginPage;
    protected ProductsPage productsPage;
    protected Logger logger;


    public LoginBaseTest(){
        loginPage = new LoginPage();
        productsPage = new ProductsPage();

        logger = TestLogger.getLogger(LoginBaseTest.class.getName());
    }


    @AfterMethod
    public void afterMethod(){

        System.out.println("afterMethod() from LoginBaseTest...");
        SeleniumTest.getDriver().close();
    }

    @AfterClass
    public void afterClass(){

        System.out.println("Afterclass() from LoginBaseTest...");

    }

}
