package com.saucelabs.tests.APIs;

import com.google.gson.JsonObject;
import com.saucelabs.common.utils.logs.TestLogger;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class POST_API_Test {

    String environment ="https://reqres.in";

    String create_User_API_Endpoint = "/api/users";

    Logger logger;

    @Test
    public void createUserTest(){

        logger = TestLogger.getLogger(POST_API_Test.class.getName());

        // Initiate Object For Rest Assured

        RequestSpecification request = RestAssured.given();

        request.header("Content-Type", "application/json");

        // Request Body
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("name","Charles");
        jsonBody.addProperty("job","Scrum Master");


        // API Call
        request.body(jsonBody.toString());

       Response response =  request.post(environment + create_User_API_Endpoint);

       // Parse the response
        JSONObject responseJson = new JSONObject(response.body().asString());

       

        // Validate the API Response
        String user_iD = responseJson.get("id").toString();
        String name = responseJson.getString("name");
        String createdTimeStamp = responseJson.get("data").toString();

        // Check points
        Assert.assertNotNull(user_iD, "user_ID is null in Created User API Response");
        Assert.assertNotNull(createdTimeStamp,"created time stamp is null in the create user api response");
        Assert.assertNotNull(name, "name field is null in the create user api response");

        logger.info("User ID from API Response: "+ user_iD);
        logger.info("user created time stamp: "+ createdTimeStamp);
        logger.info("user name in reponse is: "+name);

    }


}
