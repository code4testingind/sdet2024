package com.saucelabs.tests.APIs;

import com.saucelabs.common.utils.logs.TestLogger;
import io.restassured.RestAssured;
import io.restassured.internal.common.assertion.Assertion;
import io.restassured.internal.path.json.JSONAssertion;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GET_API_Test {

    public String environment = "https://reqres.in";

    public String LIST_USERS_API_ENDPOINT = "/api/users?page=2";

    Logger logger;

    @Test
    public void GET_API_Test(){

        logger = TestLogger.getLogger(GET_API_Test.class.getName());

        // Create rest assured object
        RequestSpecification request = RestAssured.given();

        request.header("Content-Type","application/json");

        Response response = request.get(environment + LIST_USERS_API_ENDPOINT);

        logger.info("sample response from LIst Users API" + response);

        // complete response json object
        JSONObject responseJson = new JSONObject(response.body().asString());

        logger.info("total pages from respone: "+responseJson.get("total_pages"));

        logger.info("pages att from response: " +responseJson.get("per_page"));

        Assert.assertEquals(responseJson.get("total_pages"), 2, "total pages count is not matching");


        // Ex: JSON Array: data object
       JSONArray data =  responseJson.getJSONArray("data");

       logger.info("total records available in data column: "+data.length());
       Assert.assertEquals(data.length(), 6, "total user records are not matching in Data column");

       // parsing mechanism  - process the data array
        for(int i =0; i<data.length(); i++){

            String email = data.getJSONObject(i).get("email").toString();
            Assert.assertNotNull(email, "Email is returned null");
            logger.info("email from the user: "+ email);

            logger.info("last name is: "+ data.getJSONObject(i).get("last_name").toString());
            logger.info("Avatar image url is: "+ data.getJSONObject(i).get("avatar").toString());

        }

        // JSON Ojbect Example:

       JSONObject support = (JSONObject) responseJson.get("support");
        String url = support.getString("url");
       // logger.info("support url from LIST User api response... is: "+support.get("url"));
        logger.info("support url from LIST User api response... is: "+ url);

        logger.info("support text from LIST User api response... is: "+support.get("text"));





    }
}
