package com.saucelabs.tests.assignments;

import Infra.SeleniumTest;
import com.saucelabs.testutils.BaseTest;
import org.testng.annotations.BeforeMethod;
import pages.sauelabs.assignments.IFSC_Bank_Page;

import java.io.IOException;

public class AssignmentBaseTest extends BaseTest {

    protected IFSC_Bank_Page ifsc_bank_page;

    public AssignmentBaseTest(){

         ifsc_bank_page = new IFSC_Bank_Page();
    }

    @BeforeMethod
    public void beforeMethod() throws IOException {

        System.out.println("beforeMethod() from BaseTest...");

        SeleniumTest.getInstance().launchBrowser("Chrome");
        SeleniumTest.getDriver().get(propManager.getProperty("IFSC_Bank_URL"));

    }
}
