package com.saucelabs.tests.assignments;

import org.testng.Assert;
import org.testng.annotations.Test;

public class IFSC_Code_Test extends AssignmentBaseTest{

    @Test
    public void findIFSCCode() throws InterruptedException {

        //Step-01: Bank: State Bank of India
        ifsc_bank_page.selectBank("BANK OF INDIA");

        // Step-02: State: Andhra Pradesh
        ifsc_bank_page.selectState("KARNATAKA");

        // Step-03: District:
        ifsc_bank_page.selectDistrict("BANGALORE");

        // Step-04: Branch:
        ifsc_bank_page.selectBranch("CANTONMENT");

        //Get IFSC Code
        String bank_IFSC_CODE = ifsc_bank_page.get_IFSC_Code();

        // Verify is IFSC Code is not or not
        Assert.assertNotNull(bank_IFSC_CODE, "IFSC Code is returned null value...");
    }
}
