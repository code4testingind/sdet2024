package com.saucelabs.tests.collectionsdemo;
import java.util.ArrayList;
import java.util.List;

public class ListDemoTest {


    public static void main(String[] args) {

        // List
        // 1 - we can store any data

        // 2 - List can accept duplicate value also

        List productlist = new ArrayList();

        ArrayList clientlist = new ArrayList();

        //insert the product records
        productlist.add("Iphone 11"); // 0
        productlist.add("Iphone 13"); // 1
        productlist.add("Iphone 15"); // 2
        productlist.add("Samsung s23"); // 3
        productlist.add("Motorola Razer"); // index  4
        productlist.add("Iphone 11");
        productlist.add(999.89);
        productlist.add('L');
        productlist.add(true);



        // Insert client details
        clientlist.add("Dell");
        clientlist.add("Apple");
        clientlist.add("Dell");
        clientlist.add("Microsfot");
        clientlist.add("Dell");



        //Fetech the records
        for(Object o : productlist){
            System.out.println("product list is: "+ o);
        }

        // Find the total products available - size of the list
        System.out.println("Size of the product list is or total products: "+ productlist.size() );

        //Read the values from List by using get() method
        System.out.println("customer required mobbile is : "+ productlist.get(4));


        // List - add(), size(), get() / for-each loop



    }
}
