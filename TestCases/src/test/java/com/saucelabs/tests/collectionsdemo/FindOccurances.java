package com.saucelabs.tests.collectionsdemo;

import java.util.*;

public class FindOccurances {

    public static void main(String[] args) {


        // Input list
        // [1, 2, 3, 4, 2, 5, 6, 7, 5, 8, 7, 9, 5]
        ArrayList<Integer> input = new ArrayList<Integer>();

        input.add(1);
        input.add(2);
        input.add(3);
        input.add(4);
        input.add(2);
        input.add(5);
        input.add(6);
        input.add(7);
        input.add(5);
        input.add(8);
        input.add(7);
        input.add(9);
        input.add(5);


        // 1 create object
        // ClassName objName = new ClassName();

        // Data Parsing logic

        FindOccurances test = new FindOccurances();
      //  test.findDuplicates(input);

        test.findOccurances(input);

    }

    // [1, 2, 3, 4, 2, 5, 6, 7, 5, 8, 7, 9, 5]
    public void findOccurances(List<Integer> input){

        HashMap<Integer, Integer> frequenceyMap = new HashMap<>();

        System.out.println("Total eleents in the input list: "+input.size());

        for(Integer o: input){

            if(frequenceyMap.containsKey(o)){

                frequenceyMap.put(o, frequenceyMap.get(o) + 1 );
            }else {
                frequenceyMap.put(o, 1);
            }

        }

        //Pull the values from the map
        for(Map.Entry output: frequenceyMap.entrySet()){

            System.out.println("Key is: " + output.getKey() + ", Value is: " + output.getValue());

        }


    }



    public void findDuplicates(List<Integer> list){

        HashSet set = new HashSet(); // unique elements

        List duplicateElements = new ArrayList(); // duplicate element collection

        System.out.println("total elemetns available in input list: "+list.size());


       for(Object o : list){

           if(set.add(o)){

               System.out.println("Element inserted in Set is:  "+ o );
           }else{

               duplicateElements.add(o);
           }
       }

       System.out.println("Size of the unique element collection: "+ set.size());
       System.out.println("Size of the duplicate element collection: "+ duplicateElements.size());

       //Print duplicate elements as well

        Iterator iterator = duplicateElements.iterator();

        while(iterator.hasNext()){
            System.out.println("Duplicate element is: " + iterator.next());
        }

        System.out.println("Size of the duplicate elemenet collectio: "+duplicateElements.size());
    }
}
