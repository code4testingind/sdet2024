package com.saucelabs.tests.collectionsdemo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class SetTest {

    public static void main(String[] args) {


        // List = 12, 15, 18, 20, 12, 15, 3, 4, 1
        List list = new LinkedList<>();

        list.add(12);
        list.add(15);
        list.add(18);
        list.add(20);
        list.add(12);
        list.add(15);
        list.add(3);
        list.add(4);
        list.add(1);




        // Find the duplicates by using Set
        Set set = new HashSet<>();

//        for( Object o : list){
//
//            if(set.contains(o)){
//
//                System.out.println("duplicate value is: " + o);
//            }else{
//                set.add(o);
//            }
//        }

        for(Object o : list){

            if(set.add(o)){

                System.out.println("inserted record into set is: "+ o);

            }else{
                System.out.println("Duplicte element identified: " + o);
            }
        }

    }
}
