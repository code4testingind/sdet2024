package com.saucelabs.tests.collectionsdemo;

import java.util.*;

public class Sample_TEST {

    public static void main(String[] args) {

        ArrayList<Integer> input = new ArrayList<Integer>();

        input.add(1);
        input.add(2);
        input.add(3);
        input.add(2);
        input.add(3);
        input.add(4);
        input.add(5);
        input.add(6);
        input.add(5);


//        Map<Integer,Integer> frequencyMap = new HashMap();
//
//        for(int i : input){
//
//            if(frequencyMap.containsKey(i)){
//
//                frequencyMap.put(i, frequencyMap.get(i)+1);
//            }else{
//                frequencyMap.put(i, 1);
//            }
//
//        }


       for(Object o: input){

           int count = Collections.frequency(input, o);
           System.out.println(o + " count is: "+count);
       }

        Sample_TEST test = new Sample_TEST();

        test.findduplicates(input);

        test.findOccurances(input);


    }

    public void findduplicates(List<Integer> input){

        HashSet set = new HashSet<>();

        List uniqueList = new ArrayList();

        List dulicatcateList = new ArrayList();

        for(Object o : input){

            if(set.add(o)){

                uniqueList.add(o);
            }else {
                dulicatcateList.add(o);
            }
        }

        System.out.println("dupicate elements are:");
        for(Object o: dulicatcateList){
            System.out.println("Duplicate elements are: "+ o);
        }
    }

    public void findOccurances(List<Integer> input){

        Map<Integer,Integer> outputmap = new HashMap();

     for(int j: input){

         if(outputmap.containsKey(j)){
             outputmap.put(j, outputmap.get(j)+1);
         }else{
             outputmap.put(j,1);
         }
     }

     //print the duplicate elements
       for(Map.Entry entry: outputmap.entrySet()){
           System.out.println("key is: "+entry.getKey() + ", value is: "+ entry.getValue());
       }
    }
}
