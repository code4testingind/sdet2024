package com.saucelabs.tests.collectionsdemo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListExamle1 {

    public static void main(String[] args) {

        List list = new ArrayList();

        LinkedList students = new LinkedList();

        students.add("1");
        students.add("2");
        students.add("5");
        students.add("6");
        students.add("3");

        // select.getOptions();

        // Pull the data from Collections
        /*
             - get() --- need index to pull the data

             - for-each loop

             - itertator()

             for(DataType variableName : CollectionName){

             // use temp variable in side the loop to partse the data

            }
         */

        for(Object o: students){

            System.out.println("data from LinkedList " + o);
        }
    }
}
