package com.saucelabs.tests.collectionsdemo;

import java.util.HashMap;
import java.util.Map;

public class MapsDemo {

    public static void main(String[] args) {

        // Sample map

        HashMap<Integer,Integer> students_map =new HashMap();

        students_map.put(1, 80);
        students_map.put(2, 70);
        students_map.put(3, 85);

        System.out.println("Pull data from Map "+ students_map.get(3));

        //pull the data from Map
        for(Map.Entry entry : students_map.entrySet()){

            System.out.println("Key is: "+ entry.getKey() + ", Value is: "+ entry.getValue());

        }



    }
}
